var shoppingCart = [];

var savedFormDeliveryData = [];

/*
 * Pola dla shoppingCart:
 * product_id
 * product_name
 * product_quantity
 * product_price
 */

function addToCart(product_id, is_single_element) {
    var cartItem = {
        cart_product_id : product_id,
        cart_product_name : products[product_id].product_name,
        cart_product_quantity : 1,
        cart_product_price : products[product_id].product_price
    };

    if(!is_single_element)
        cartItem.cart_product_quantity = parseInt(document.getElementById('inputQuantity').value);

    if(parseInt(products[product_id].product_quantity) <= 0)
        products[product_id].product_quantity = 0;

    if(parseInt(cartItem.cart_product_quantity) > parseInt(products[product_id].product_quantity))
        alert("Została przekroczona ilość dostępnych sztuk produktu.");

    else {
        products[product_id].product_quantity -= parseInt(cartItem.cart_product_quantity);

        shoppingCart.push(cartItem);

        localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart));
    }
}

function deleteDuplicatesInCart() {
    if(shoppingCart.length >= 2) {
        for(var i = 0; i < shoppingCart.length - 1; i++) {
            for(var j = i+1; j < shoppingCart.length; j++) {
                if(shoppingCart[i].cart_product_id === shoppingCart[j].cart_product_id) {
                    shoppingCart[i].cart_product_quantity += parseInt(shoppingCart[j].cart_product_quantity);

                    shoppingCart.splice(j, 1);
                }
            }
        }
    }
}

function deleteFromCart(index) {
    for(var i = 0; i < products.length; i++)
        if(products[i].product_id == shoppingCart[index].cart_product_id) {
            products[i].product_quantity += parseInt(shoppingCart[index].cart_product_quantity);

            shoppingCart.splice(index, 1);

            localStorage.setItem('shoppingCart', JSON.stringify(shoppingCart));

            break;
        }
}

function cartTotalValue() {
    var totalValue = 0.00;      //całkowita wartość koszyka
    var totalItemsInCart = 0;   //ilość przedmiotów w koszyku

    for(var i = 0; i < shoppingCart.length; i++) {
        totalValue += shoppingCart[i].cart_product_quantity * shoppingCart[i].cart_product_price;
        totalItemsInCart += parseInt(shoppingCart[i].cart_product_quantity);
    }

    document.getElementById('cartItemsQuantity').innerHTML = totalItemsInCart;

    document.getElementById('cartTotalValue').innerHTML = totalValue.toFixed(2) + " zł";
}

function loadData() {
    const xhr = new XMLHttpRequest();
    xhr.open("GET", "/productData.json", true);
    xhr.onload = (e) => {
        if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                products = xhr.responseText;
                products = JSON.parse(products).products;
                randomize();
                document.getElementById('page_content').innerHTML = showHomepage();

            }
        }
    }
    xhr.send(null);
}

function showSite(id, product_id)
{
    var content="";

    switch (id)
    {
        case 1:
            //content += showHomepage();
            loadData();
            return;
        case 2:
            content += showAboutShop();
            break;
        case 3:
            content += showAllProducts();
            break;
        case 4:
            content += showPopularProducts();
            break;
        case 5:
            content += showNewProducts();
            break;
        case 6:
            content += showContact();
            break;
        case 7:
            content += showItem(product_id);
            break;
        case 8:
            deleteDuplicatesInCart();
            content += showShoppingCart();
            break;
        case 9:
            content += showDeliveryDataForm();
            break;
        case 10:
            content += showOrderSummary();
            break;
        case 11:
            shoppingCart.length = 0;
            cartTotalValue();
            content += orderCompleted();
            break;
        default:
            content += showHomepage();
    }

    document.getElementById('page_content').innerHTML = content;
}

function showItem(id)
{
    document.title = "Sakura World - " + products[id].product_name;

    var temp_id = parseInt(id) + 1;

    var item_empty_star = 5;

    var content = `<!-- Product section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <div class="col-md-6">
                        <a href="` + products[id].product_image_href + `" data-lightbox="Galeria" data-title="` + products[id].product_name + `">
                            <img class="card-img-top mb-5 mb-md-0" width="588" height="800" src="` + products[id].product_thumbnail_href + `" alt="..." />
                        </a>
                    </div>
                    <div class="col-md-6">
                        <div class="small mb-1">ID: ` + products[id].product_id + `</div>
                        <h1 class="display-5 fw-bolder">` + products[id].product_name + `</h1>
                        <div class="fs-5 mb-5">
                            <span>Cena: ` + products[id].product_price.toFixed(2) + ` zł</span>
                        </div>
                        <p class="lead">` + products[id].product_description + `</p>

                        <!-- Product reviews-->
                        <div class="d-flex fs-5 text-warning mb-5">
                        <span class="mb-5" style="color: #000000;">Ocena: </span>
                        <div class="mx-2"></div>`;

                        for(var j = 0; j < products[id].product_rating; j++) {
                            content += `<div class="bi-star-fill"></div>`;
                            item_empty_star--;
                        }

                        for(var k = 0; k < item_empty_star; k++) {
                            content += `<div class="bi-star-fill bi-star-empty"></div>`;
                        }

                        content += `</div>`;

                        content += `<div class="d-flex">
                            <input class="form-control text-center me-3" id="inputQuantity" type="number" value="1" min="1" max="` + products[id].product_quantity + `" style="max-width: 5rem" />
                            <a class="btn btn-outline-dark mt-auto" href="#" id="cart_button_link" onclick="addToCart(`+ products[id].product_id +`, false); showSite(7,` + products[id].product_id + `); cartTotalValue()">
                                <i class="bi-cart-fill me-1"></i>
                                <span id="cart_button_content">`;
                                if(parseInt(products[id].product_quantity) > 0)
                                    content += `Dodaj do koszyka`;
                                else
                                    content += `Wyprzedane`;
                                content += `</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Related items section-->
        <section class="py-5 bg-light">
            <div class="container px-4 px-lg-5 mt-5">
                <h2 class="fw-bolder mb-4">Podobne produkty:</h2>
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">`;

                for(var i = 0; i < 4; i++)
                {
                    if(temp_id >= products.length)
                        temp_id = 0;

                    if(temp_id === id)
                        temp_id = id + 1;

                    var empty_star = 5;

                    content += `<div class="col mb-5">
                        <div class="card h-100">
                            <!-- Product image-->
                            <img class="card-img-top" width="269" height="380" src="` + products[temp_id].product_image_href + `" alt="..." />
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <a href="#!" onclick="showSite(7, `+ products[temp_id].product_id +`)">
                                    <div class="text-center">
                                        <!-- Product name-->
                                        <h5 class="fw-bolder">` + products[temp_id].product_name + `</h5>
                                        <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">`;

                                                    for(var j = 0; j < products[temp_id].product_rating; j++) {
                                                        content += `<div class="bi-star-fill"></div>`;
                                                        empty_star--;
                                                    }

                                                    for(var k = 0; k < empty_star; k++)
                                                        content += `<div class="bi-star-fill bi-star-empty"></div>`;

                                                content += `</div>
                                        <!-- Product price-->
                                        ` + products[temp_id].product_price.toFixed(2) + ` zł
                                    </div>
                                </a>
                            </div>
                            <!-- Product actions-->
                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#"onclick="addToCart(`+ products[i].product_id +`, true); cartTotalValue()">Dodaj do koszyka</a></div>
                            </div>
                        </div>
                    </div>`;

                    temp_id++;
                }

                content += `</div>
                </div>
        </section>`;

        return content;
}

function showHomepage()
{
    document.title = "Sakura World - Twoje okno na świat Japonii";
    var content = `<!-- Section-->
            <section class="py-5">
                <div class="container px-4 px-lg-5 mt-5">
                    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">`;

                        for(var i = 0; i < 8; i++)
                        {
                            var id = window.random_index[i];
                            var empty_star = 5;

                            content += `<div class="col mb-5">
                                <div class="card h-100">
                                    <a href="#!" onclick="showSite(7, '` + products[id].product_id + `')">
                                        <!-- Product image-->
                                        <img class="card-img-top" width="269" height="380" src="` + products[id].product_image_href + `" alt="..." />
                                        <!-- Product details-->
                                        <div class="card-body p-4">
                                            <div class="text-center">
                                                <!-- Product name-->
                                                <h5 class="fw-bolder">` + products[id].product_name + `</h5>
                                                <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">`;

                                                    for(var j = 0; j < products[id].product_rating; j++) {
                                                        content += `<div class="bi-star-fill"></div>`;
                                                        empty_star--;
                                                    }
                                                    for(var k = 0; k < empty_star; k++) {
                                                        content += `<div class="bi-star-fill bi-star-empty"></div>`;
                                                    }
                                                content += `</div>
                                                <!-- Product price-->
                                                ` + products[id].product_price.toFixed(2) + ` zł
                                            </div>
                                        </div>
                                    </a>
                                    <!-- Product actions-->
                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#" onclick="addToCart(`+ products[id].product_id +`, true); deleteDuplicatesInCart(); cartTotalValue()">Dodaj do koszyka</a></div>
                                    </div>
                                </div>
                            </div>`;
                        }
                   content += `</div>
                </div>
            </section>`;

            return content;
}

function showAboutShop()
{
        document.title = "Sakura World - O nas";

        var content = "";

        content = `<section class="py-5">
        <h2 class="fw-bolder mb-5 text-center w-100">O nas</h2>
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <p class="lead"><span class="fw-bolder">Sakura World</span> to prawdziwy raj dla miłośników mang. W naszym sklepie zanurzysz się w fascynującym świecie japońskiej kultury komiksów. Odkryjesz tu nie tylko najpopularniejsze tytuły, ale także ukryte perełki, które przeniosą Cię w niezwykłe przygody bohaterów i zapewnią chwile pełne emocji i wrażeń. Od klasycznych serii po najnowsze wydawnictwa, oferujemy szeroki wachlarz mang z różnorodnymi gatunkami i stylami graficznymi. Niezależnie od tego, czy preferujesz shōnen, shōjo, seinen czy yaoi, u nas znajdziesz coś dla siebie. Przyjdź do naszego sklepu i przekonaj się, jakie wspaniałe światy czekają na Ciebie wśród naszych półek. Odkryj magię mang w Sakura World!</p>
                </div>
            </div>
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <div class="col mb-5">
                        <p class="lead">
                            <span class="fw-bolder">ADRES:</span>
                        </p>
                        <p class="lead">
                            "SAKURA WORLD" Magdalena Wawryniuk<br/>
                            Ul. Fiołkowa 5<br/>
                            20-834 Lublin
                        </p>

                        <p class="lead">
                            <span class="fw-bolder">NIP:</span>
                        </p>

                        <p class="lead">
                            4206662137
                        </p>

                        <p class="lead">
                            <span class="fw-bolder">NUMER KONTA DO PRZELEWÓW:</span>
                        </p>

                        <p class="lead">ING Bank Śląski: 12 3456 7890 1234 5678 9012 3456</p>
                    </div>
                    <div class="col mb-5">`;
                        getLocation();
                        content+= `<div id="map" style="width: 580px; height: 350px;"></div>
                    </div>
                </div>
            </div>
            <h4 class="fw-bolder mb-5 text-center w-100">Znajdź nas na mediach społecznościowych!</h4>
            <div class="container px-4 px-lg-4 px-md-2 my-5">
                <div class="row gx-4 gx-lg-4 align-items-center">
                    <div class="col">
                        <a class="tile facebook" href="https://facebook.com" target="_blank" title="Znajdź nas na Facebooku!">
                            <i class="demo-icon icon-facebook-1"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile instagram" href="https://instagram.com" target="_blank" title="Znajdź nas na Instagramie!">
                            <i class="demo-icon icon-instagram"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile twitter" href="https://twitter.com" target="_blank" title="Znajdź nas na Twitterze!">
                            <i class="demo-icon icon-twitter"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile card-img-top linkedin" href="https://linkedin.com" target="_blank" title="Znajdź nas na LinkedIn!">
                            <i class="demo-icon icon-linkedin"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile youtube" href="https://youtube.com" target="_blank" title="Znajdź nas na YouTube!">
                            <i class="demo-icon icon-youtube"></i>
                        </a>
                    </div>
                </div>
            </div>
        </secton>`;

        return content;
}

function showAllProducts()
{
        document.title = "Sakura World - Wszystkie produkty";

        var content = `<!-- Section-->
            <section class="py-5">
            <h2 class="fw-bolder mb-5 text-center w-100">Wszystkie produkty</h2>
                <div class="container px-4 px-lg-5 mt-5">
                    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">`;

                        for(var i = 0; i < products.length; i++)
                        {
                            var empty_star = 5;
                            content += `<div class="col mb-5">
                                <div class="card h-100">
                                    <a href="#!" onclick="showSite(7, '` + products[i].product_id + `')">
                                        <!-- Product image-->
                                        <img class="card-img-top" width="269" height="380" src="` + products[i].product_image_href + `" alt="..." />
                                        <!-- Product details-->
                                        <div class="card-body p-4">
                                            <div class="text-center">
                                                <!-- Product name-->
                                                <h5 class="fw-bolder">` + products[i].product_name + `</h5>
                                                <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">`;

                                                    for(var j = 0; j < products[i].product_rating; j++) {
                                                        content += `<div class="bi-star-fill"></div>`;
                                                        empty_star--;
                                                    }

                                                    for(var k = 0; k < empty_star; k++)
                                                        content += `<div class="bi-star-fill bi-star-empty"></div>`;

                                                content += `</div>
                                                <!-- Product price-->
                                                ` + products[i].product_price.toFixed(2) + ` zł
                                            </div>
                                        </div>
                                    </a>
                                    <!-- Product actions-->
                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#" onclick="addToCart(`+ products[i].product_id +`, true); cartTotalValue()">Dodaj do koszyka</a></div>
                                    </div>
                                </div>
                            </div>`;
                        }
                   content += `</div>
                </div>
            </section>`;

        return content;
}

function showPopularProducts()
{
        document.title = "Sakura World - Bestsellery";

        var content = `<!-- Section-->
            <section class="py-5">
            <h2 class="fw-bolder mb-5 text-center w-100">Bestsellery</h2>
                <div class="container px-4 px-lg-5 mt-5">
                    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">`;

                        for(var i = 0; i < products.length; i++)
                        {
                            if(products[i].product_rating === 5) {
                                content += `<div class="col mb-5">
                                <div class="card h-100">
                                    <a href="#!" onclick="showSite(7, '` + products[i].product_id + `')">
                                        <!-- Product image-->
                                        <img class="card-img-top" width="269" height="380" src="` + products[i].product_image_href + `" alt="..." />
                                        <!-- Product details-->
                                        <div class="card-body p-4">
                                            <div class="text-center">
                                                <!-- Product name-->
                                                <h5 class="fw-bolder">` + products[i].product_name + `</h5>
                                                <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">`;

                                                    for(var j = 0; j < products[i].product_rating; j++)
                                                        content += `<div class="bi-star-fill"></div>`;

                                                content += `</div>
                                                <!-- Product price-->
                                                ` + products[i].product_price.toFixed(2) + ` zł
                                            </div>
                                        </div>
                                    </a>
                                    <!-- Product actions-->
                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#" onclick="addToCart(`+ products[i].product_id +`, true); cartTotalValue()">Dodaj do koszyka</a></div>
                                    </div>
                                </div>
                            </div>`;
                            }
                        }
                   content += `</div>
                </div>
            </section>`;

        return content;
}

function showNewProducts()
{
        document.title = "Sakura World - Nowości";

        var content = `<!-- Section-->
            <section class="py-5">
            <h2 class="fw-bolder mb-5 text-center w-100">Nowości</h2>
                <div class="container px-4 px-lg-5 mt-5">
                    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">`;

                        for(var i = products.length-1; i >= 7; i--)
                        {
                            var empty_star = 5;
                            content += `<div class="col mb-5">
                                <div class="card h-100">
                                    <a href="#!" onclick="showSite(7, '` + products[i].product_id + `')">
                                        <!-- Product image-->
                                        <img class="card-img-top" width="269" height="380" src="` + products[i].product_image_href + `" alt="..." />
                                        <!-- Product details-->
                                        <div class="card-body p-4">
                                            <div class="text-center">
                                                <!-- Product name-->
                                                <h5 class="fw-bolder">` + products[i].product_name + `</h5>
                                                <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">`;

                                                    for(var j = 0; j < products[i].product_rating; j++) {
                                                        content += `<div class="bi-star-fill"></div>`;
                                                        empty_star--;
                                                    }
                                                    for(var k = 0; k < empty_star; k++)
                                                        content += `<div class="bi-star-fill bi-star-empty"></div>`;

                                                content += `</div>
                                                <!-- Product price-->
                                                ` + products[i].product_price.toFixed(2) + ` zł
                                            </div>
                                        </div>
                                    </a>
                                    <!-- Product actions-->
                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#" onclick="addToCart(`+ products[i].product_id +`, true); cartTotalValue()">Dodaj do koszyka</a></div>
                                    </div>
                                </div>
                            </div>`;
                        }
                   content += `</div>
                </div>
            </section>`;

        return content;
}

function showContact()
{
    document.title = "Sakura World - Kontakt";

    var content = `<!-- Section-->
    <section class="py-5">
        <h2 class="fw-bolder mb-5 text-center w-100">Masz jakieś pytania? Skontaktuj się z nami!</h2>
        <div class="container px-4 px-lg-5 mt-5">
            <div class="row gx-4 gx-md-3 gx-sm-1 justify-content-center">
                <form action="mailto:mwawryniuk01@gmail.com" method="post" onsubmit="return validateData()">
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="red">*</span> Imię i nazwisko:
                                </td>
                                <td>
                                    <input type="text" id="name_surname" name="name_surname" placeholder="Jan Kowalski" title="Wprowadź swoje imię i nazwisko" required />
                                </td>
                                <td id="name_surname_error" class="red"></td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="red">*</span> Adres e-mail:
                                </td>
                                <td>
                                    <input type="text" id="email" name="email" placeholder="example@example.com" title="Wprowadź swój adres e-mail" required />
                                </td>
                                <td id="email_error" class="red"></td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="radio" name="want_add_phone_number" value="yes" required onchange="togglePhoneField(true)"> Chcę podać swój numer telefonu
                                </td>
                                <td>
                                    <input type="radio" name="want_add_phone_number" value="no" required onchange="togglePhoneField(false)"> Nie chcę podawać numeru telefonu
                                </td>
                                <td id="want_add_phone_number_error" class="red"></td>
                                </tr>
                                <tr>
                                    <td id="add_phone_number">
                                        Numer telefonu:
                                    </td>
                                    <td>
                                        <input type="text" id="phone_number" name="phone_number" disabled />
                                    </td>
                                    <td id="phone_number_error" class="red"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="red">*</span> Temat wiadomości:
                                    </td>
                                    <td>
                                        <select size="1" id="message_subject" name="message_subject" name="message_subject" required>
                                            <option value="" selected="selected">Wybierz temat wiadomości</option>
                                            <option>Reklamacje</option>
                                            <option>Zwrot</option>
                                            <option>Status zamówienia</option>
                                            <option>Dostawa</option>
                                            <option>Produkty</option>
                                            <option>Inny</option>
                                        </select>
                                    </td>
                                    <td id="message_subject_error" class="red"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="red">*</span> Wiadomość:
                                    </td>
                                    <td>
                                        <textarea name="message" id="message" placeholder="Treść wiadomości" cols="40" rows="4" title="Wprowadź treść wiadomości" required></textarea>
                                    </td>
                                    <td id="message_error" class="red"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="checkbox" id="checkbox" required><span class="red">*</span> Wyrażam zgodę na przetwarzanie przez Sprzedawcę moich danych osobowych zawartych w formularzu w celu udzielenia odpowiedzi na zadane poprzez formularz pytania.
                                    </td>
                                    <td id="checkbox_error" class="red"></td>
                                </tr>
                            <tr>
                                <td colspan="2">
                                    <button type="submit" class="button btn btn-outline-dark float-start">Wyślij</button>
                                    <button type="reset" class="button btn btn-outline-dark float-end">Wyczyść</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>

        <div class="container px-4 px-lg-5 mt-5">
            <button class="button btn btn-outline-dark" onclick="showElementById('formData'); showElementById('tableFormData'); displaySavedFormData()">Pokaż dane z formularza</button>
            <button class="button btn btn-outline-dark" onclick="hideElementById('formData')">Ukryj dane z formularza</button>
            <div id="formData" style="display: none;" class="mt-5">
                <div class="row gx-4 gx-md-3 gx-sm-1 justify-content-center" id="tableFormData">
                </div>

                <button class="button btn btn-outline-dark" onclick="showElementById('tableFormData'); displaySavedFormData()">Wyświetl zapisane dane formularza</button>
                <button class="button btn btn-outline-dark" onclick="hideElementById('tableFormData')">Ukryj zapisane dane formularza</button>
                <button class="button btn btn-outline-dark" onclick="clearFormData()">Wyczyść wszystkie dane formularza</button>
            </div>
        </div>
    </section>`;

  return content;
}

function showFormData()
{
    var content="Następujące dane zostaną wysłane:\n";
    content += "Imię i nazwisko: " + document.getElementById('name_surname').value + "\n";
    content += "Email: "+document.getElementById('email').value+"\n";
    content += "Czy chcę podać numer telefonu? ";

    var temp = document.getElementsByName("phone_number");
    var checkedValue = "";

    for(let i = 0; i < temp.length; i++)
        if(temp[i].checked)
            checkedValue = temp[i].value;

    if(checkedValue === "yes")
    {
        content += "Tak \n";
        content += "Telefon: "+document.getElementById('phone').value + "\n";
    }
    else
        content += "Nie \n";

    content += "Temat wiadomości: " + document.getElementById('message_subject').value + "\n";
    content += "Treść wiadomości: " + document.getElementById('message').value + "\n";

    //alert(content);
}

function showFormDeliveryData()
{
    var content="Następujące dane zostaną wysłane:\n";
    content += "Imię i nazwisko: " + document.getElementById('name_surname').value + "\n";
    content += "Email: "+document.getElementById('email').value+"\n";
    content += "Telefon: "+document.getElementById('phone_number').value + "\n";
    content += "Adres: " + document.getElementById('address').value + "\n";
    content += "Kod pocztowy: " + document.getElementById('zip_code').value + "\n";
    content += "Miasto: " + document.getElementById('city').value + "\n";
    content += "Rodzaj dostawy: " + document.getElementById('delivery_method').value + "\n";

    //alert(content);
}

function saveFormDeliveryData() {
    var formField = {};

    formField.name_surname = document.getElementById('name_surname').value;
    formField.email = document.getElementById('email').value;
    formField.phone_number = document.getElementById('phone_number').value;
    formField.address = document.getElementById('address').value;
    formField.zip_code = document.getElementById('zip_code').value;
    formField.city = document.getElementById('city').value;
    formField.delivery_method = document.getElementById('delivery_method').value;

    var existingItems = localStorage.getItem('savedFormDeliveryData');
    savedFormDeliveryData = existingItems ? JSON.parse(existingItems) : [];

    savedFormDeliveryData.push(formField);

    localStorage.setItem('savedFormDeliveryData', JSON.stringify(savedFormDeliveryData));

    document.getElementById('name_surname').value = "";
    document.getElementById('email').value = "";
    document.getElementById('phone_number').value = "";
    document.getElementById('address').value = "";
    document.getElementById('zip_code').value = "";
    document.getElementById('city').value = "";
    document.getElementById('delivery_method').value = "";
}

function updateCart(index)
{
    shoppingCart[index].cart_product_quantity = parseInt(document.getElementById('change_item_quantity_' + index).value);
}

function countItemsInCart()
{
    var totalItems = 0;

    for(var i = 0; i < shoppingCart.length; i++)
        totalItems += parseInt(shoppingCart[i].cart_product_quantity);

    return totalItems;
}

function countTotalCost()
{
    var totalCost = 0;

    for(var i = 0; i < shoppingCart.length; i++)
        totalCost += parseInt(shoppingCart[i].cart_product_quantity) * parseFloat(shoppingCart[i].cart_product_price.toFixed(2));

    return totalCost.toFixed(2);
}

function showShoppingCart()
{
    document.title = "Sakura World - Twój koszyk";

    var totalShoppingCartValue = 0;
    var allItemsInShoppingCart = 0;

    var content = `<!-- Shopping Cart Section -->
    <section class="py-5">
        <div class="container px-4 px-lg-5 mt-5">
            <h2 class="fw-bolder mb-4">Twój koszyk</h2>

            <!-- Cart items -->
            <div class="row">`
                if(shoppingCart.length === 0)
                    content += `<p>Twój koszyk jest pusty.</p>`;
                else
                {
                content += `<div class="col-lg-8">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Produkt</th>
                                    <th scope="col">Cena</th>
                                    <th scope="col">Ilość</th>
                                    <th scope="col">Łącznie</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- Cart items will be dynamically added here -->`;

                                for(var i = 0; i < shoppingCart.length; i++) {
                                    var totalCost = shoppingCart[i].cart_product_quantity * shoppingCart[i].cart_product_price.toFixed(2);
                                    content += `
                                    <tr>
                                        <td>` + shoppingCart[i].cart_product_name + `</td>
                                        <td>` + shoppingCart[i].cart_product_price.toFixed(2) + ` zł</td>
                                        <td>
                                            <input class="form-control text-center" type="number" id="change_item_quantity_` + i +`" value="` + shoppingCart[i].cart_product_quantity + `" min="1" max="` + products[shoppingCart[i].cart_product_id].product_quantity + `" oninput="updateCart(` + i + `); cartTotalValue(); showSite(8)" />
                                        </td>
                                        <td>` + totalCost.toFixed(2) + ` zł</td>
                                        <td>
                                            <button class="btn btn-sm btn-danger" onclick="deleteFromCart(` + i + `); showSite(8)">Usuń</button>
                                        </td>
                                    </tr>`;
                                    }
                                content += `
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Podsumowanie zamówienia</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Łączna ilość produktów:</span>
                                    <span>` + countItemsInCart() + `</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Łączna cena:</span>
                                    <span>` + countTotalCost() + ` zł</span>
                                </li>
                            </ul>
                            <button class="btn btn-secondary btn-block float-start" onclick="showSite(1)">Powrót do strony głównej</button>
                            <button class="btn btn-primary btn-block float-end" onclick="cartTotalValue(); showSite(9)">Przejdź dalej</button>
                        </div>
                    </div>
                </div>`;
                }
            content += `</div>
        </div>
    </section>`;

    content += `<!-- Related items section-->
        <section class="py-5 bg-light">
            <div class="container px-4 px-lg-5 mt-5">
                <h2 class="fw-bolder mb-4">Polecane produkty:</h2>
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">`;

                for(var i = 0; i < 4; i++)
                {
                    var id = random_index[i];

                    var empty_star = 5;

                    content += `<div class="col mb-5">
                        <div class="card h-100">
                            <!-- Product image-->
                            <img class="card-img-top" width="269" height="380" src="` + products[id].product_image_href + `" alt="..." />
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <a href="#!" onclick="showSite(7, `+ products[id].product_id +`)">
                                    <div class="text-center">
                                        <!-- Product name-->
                                        <h5 class="fw-bolder">` + products[id].product_name + `</h5>
                                        <!-- Product reviews-->
                                                <div class="d-flex justify-content-center small text-warning mb-2">`;

                                                    for(var j = 0; j < products[id].product_rating; j++) {
                                                        content += `<div class="bi-star-fill"></div>`;
                                                        empty_star--;
                                                    }
                                                    for(var k = 0; k < empty_star; k++)
                                                        content += `<div class="bi-star-fill bi-star-empty"></div>`;

                                                content += `</div>
                                        <!-- Product price-->
                                        ` + products[id].product_price.toFixed(2) + ` zł
                                    </div>
                                </a>
                            </div>
                            <!-- Product actions-->
                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#" onclick="addToCart(`+ products[id].product_id +`, true); cartTotalValue(); showSite(8)">Dodaj do koszyka</a></div>
                            </div>
                        </div>
                    </div>`;
                }

                content += `</div>
                </div>
        </section>`;

    return content;
}

function showDeliveryDataForm()
{
    document.title = "Sakura World - Dane adresowe i opcje dostawy";

    var content = `<!-- Adres -->
    <section class="py-5">
        <div class="container px-4 px-lg-5 mt-5">
            <div class="row">
                <!-- Cart items -->
                <div class="col">
                <!-- Formularz z danymi adresowymi i opcjami dostawy -->
                <form>
                    <h2 class="fw-bolder mb-4">Dane adresowe</h2>
                    <!-- Pola formularza -->
                    <div class="mb-3">
                        <label for="name_surname" class="form-label">Imię i nazwisko</label>
                        <input type="text" class="form-control" id="name_surname" placeholder="Jan Kowalski" required>
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Adres email</label>
                        <input type="email" class="form-control" id="email" placeholder="example@example.com" required>
                    </div>
                    <div class="mb-3">
                        <label for="phone_number" class="form-label">Numer telefonu</label>
                        <input type="email" class="form-control" id="phone_number" placeholder="123-456-789">
                    </div>
                    <div class="mb-3">
                        <label for="address" class="form-label">Adres</label>
                        <input type="text" class="form-control" id="address" placeholder="ul. Kwiatowa 4" required>
                    </div>
                    <div class="mb-3">
                        <label for="zip_code" class="form-label">Kod pocztowy</label>
                        <input type="text" class="form-control" id="zip_code" placeholder="12-345" required>
                    </div>
                    <div class="mb-3">
                        <label for="city" class="form-label">Miasto</label>
                        <input type="text" class="form-control" id="city" placeholder="Lublin" required>
                    </div>
                    <h4 class="fw-bolder mb-4 mt-4">Opcje dostawy</h4>
                    <div class="mb-3">
                        <label for="deliveryMethod" class="form-label">Rodzaj dostawy</label>
                        <select class="form-select" id="delivery_method" required>
                            <option>Wysyłka pobraniowa</option>
                            <option>Odbiór osobisty</option>
                        </select>
                    </div>
                     <!-- Przycisk "Przejdź do podsumowania" -->
                    <button class="btn btn-primary float-end" onclick="validateDeliveryData(); showSite(10)">Przejdź do podsumowania</button>
                    <!-- Przycisk "Cofnij" -->
                    <button class="btn btn-secondary float-start" onclick="showSite(8)">Cofnij</button>
                </form>
                </div>
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Podsumowanie zamówienia</h5>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Łączna ilość produktów:</span>
                                    <span>` + countItemsInCart() + `</span>
                                </li>
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    <span>Łączna cena:</span>
                                    <span>` + countTotalCost() + ` zł</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>`;

    return content;
}

function orderCompleted()
{
    document.title = "Sakura World - Potwierdzenie złożenia zamówienia";

        var content = "";

        content = `<section class="py-5">
        <h2 class="fw-bolder mb-5 text-center w-100">Udało się! Proces składania zamówienia zakończony sukcesem!</h2>
            <div class="container px-4 px-lg-5 my-5">
                <div class="row gx-4 gx-lg-5 align-items-center">
                    <p class="lead">Niebawem otrzymasz potwierdzenie złożenia zamówienia na podany adres e-mail, a w kolejnych wiadomościach numer przesyłki oraz informacje na temat aktualnego statusu zamówienia. Dziękujemy za zrobienie zakupów w Sakura World i zapraszamy ponownie!</p>
                </div>
            </div>
            <h4 class="fw-bolder mb-5 text-center w-100">Znajdź nas na mediach społecznościowych!</h4>
            <div class="container px-4 px-lg-4 px-md-2 my-5">
                <div class="row gx-4 gx-lg-4 align-items-center">
                    <div class="col">
                        <a class="tile facebook" href="https://facebook.com" target="_blank" title="Znajdź nas na Facebooku!">
                            <i class="demo-icon icon-facebook-1"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile instagram" href="https://instagram.com" target="_blank" title="Znajdź nas na Instagramie!">
                            <i class="demo-icon icon-instagram"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile twitter" href="https://twitter.com" target="_blank" title="Znajdź nas na Twitterze!">
                            <i class="demo-icon icon-twitter"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile card-img-top linkedin" href="https://linkedin.com" target="_blank" title="Znajdź nas na LinkedIn!">
                            <i class="demo-icon icon-linkedin"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a class="tile youtube" href="https://youtube.com" target="_blank" title="Znajdź nas na YouTube!">
                            <i class="demo-icon icon-youtube"></i>
                        </a>
                    </div>
                </div>
            </div>
        </secton>`;

        return content;
}

function showOrderSummary()
{
    document.title = "Sakura World - Podsumowanie";

    var content = `<!-- Opcje dostawy -->
    <section class="py-5">
        <h2 class="fw-bolder mb-4 text-center w-100">Podsumowanie</h2>
        <div class="container px-4 px-lg-5 mt-5 mb-2">
                <div class="row gx-4 gx-md-3 gx-sm-1 justify-content-center">
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-body mb-4">
                                <h5 class="card-title text-center w-100">Dane adresowe:</h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>Imię i nazwisko: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].name_surname + `</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>E-mail: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].email + `</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>Telefon: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].phone_number + `</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>Adres: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].address + `</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>Kod pocztowy: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].zip_code + `</span>
                                    </li>
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>Miasto: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].city + `</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title text-center w-100">Dostawa:</h5>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <span>Rodzaj dostawy: </span>
                                        <span>` + savedFormDeliveryData[savedFormDeliveryData.length-1].delivery_method + `</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row gx-4 gx-md-3 gx-sm-1 justify-content-center">
                    <div class="col-lg-12 mt-3 mb-3">
                        <div class="card">
                            <div class="table-responsive mb-3">
                                <h5 class="card-title text-center w-100 mt-3 mb-3">Zamówione produkty:</h5>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Produkt</th>
                                            <th scope="col">Cena</th>
                                            <th scope="col">Ilość</th>
                                            <th scope="col">Łącznie</th>
                                        </tr>
                                    </thead>
                                    <tbody>`;
                                    for(var i = 0; i < shoppingCart.length; i++) {
                                        var totalCost = shoppingCart[i].cart_product_quantity * shoppingCart[i].cart_product_price.toFixed(2);
                                        content += `
                                            <tr>
                                                <td>` + shoppingCart[i].cart_product_name + `</td>
                                                <td>` + shoppingCart[i].cart_product_price.toFixed(2) + ` zł</td>
                                                <td>` + shoppingCart[i].cart_product_quantity + `</td>
                                                <td>` + totalCost.toFixed(2) + ` zł</td>
                                            </tr>`;
                                    }
                                    content += `
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Przycisk "Przejdź do podsumowania" -->
                <button class="btn btn-primary float-end" onclick="showSite(11)">Złóż zamówienie</button>
                <!-- Przycisk "Cofnij" -->
                <button class="btn btn-secondary float-start" onclick="showSite(9)">Cofnij</button>
            </div>
        </div>
    </section>`;

    return content;
}

function togglePhoneField(enable)
{
    var phoneField = document.getElementById("phone_number");

    var addPhoneNumber = document.getElementById("add_phone_number");

    if(enable)
    {
        phoneField.disabled = false;
        phoneField.value = "";
        phoneField.setAttribute("required", "required");
        addPhoneNumber.innerHTML = '<span class="red">*</span> Numer telefonu:';
        phoneField.placeholder = "123-456-789";
        phoneField.title = "Wprowadź swój numer telefonu w formacie 123-456-789";
    }
    else
    {
        phoneField.disabled = true;
        phoneField.value = "Pole nieaktywne";
        phoneField.removeAttribute("required");
        addPhoneNumber.innerHTML = 'Numer telefonu:';
        phoneField.placeholder = "";
        phoneField.title = "";
    }
}

function saveFormData() {
    var formField = {};

    formField.name_surname = document.getElementById('name_surname').value;
    formField.email = document.getElementById('email').value;
    formField.phone_number = document.getElementById('phone_number').value;
    formField.message_subject = document.getElementById('message_subject').value;
    formField.message = document.getElementById('message').value;

    var existingItems = localStorage.getItem('savedFormData');
    var savedFormData = existingItems ? JSON.parse(existingItems) : [];

    savedFormData.push(formField);

    localStorage.setItem('savedFormData', JSON.stringify(savedFormData));

    document.getElementById('name_surname').value = "";
    document.getElementById('email').value = "";
    document.getElementById('phone_number').value = "";
    document.getElementById('message_subject').value = "";
    document.getElementById('message').value = "";
}

function displaySavedFormData() {
    var outputDiv = document.getElementById('tableFormData');
    outputDiv.innerHTML = '';

    var existingItems = localStorage.getItem('savedFormData');

    if (existingItems) {
        var items = JSON.parse(existingItems);

        if(items.length === 0) {
            document.getElementById('tableFormData').innerHTML = '<h3 class="fw-bolder mb-5 mt-5 text-center w-100">Brak zapisanych danych formularza.</h3>';
            return;
        }

        document.getElementById('tableFormData').innerHTML = '<h3 class="fw-bolder mb-5 mt-5 text-center w-100">Zapisane dane formularza kontaktowego:</h3>';

        var table = document.createElement('table');
        table.innerHTML = '<tr><th>Imię i nazwisko</th><th>Adres e-mail</th><th>Numer telefonu</th><th>Temat wiadomości</th><th>Treść wiadomości</th><th>Akcje</th></tr>';

        for (var i = 0; i < items.length; i++) {
            var row = document.createElement('tr');
            row.innerHTML = '<td>' + items[i].name_surname + '</td>' +
            '<td>' + items[i].email + '</td>' +
            '<td>' + items[i].phone_number + '</td>' +
            '<td>' + items[i].message_subject + '</td>' +
            '<td>' + items[i].message + '</td>' +
            '<td><button onclick="editSavedFormData(' + i + ')">Edytuj</button>' +
            '<button onclick="removeSavedFormData(' + i + ')">Usuń</button></td>';
            table.appendChild(row);
        }

        outputDiv.appendChild(table);
    }
    else {
        document.getElementById('tableFormData').innerHTML = '<h3 class="fw-bolder mb-5 mt-5 text-center w-100">Brak zapisanych danych formularza.</h3>';
    }
}

function editSavedFormData(index) {
    var existingItems = localStorage.getItem('savedFormData');
    if (!existingItems)
        return;

    var items = JSON.parse(existingItems);
    if (index < 0 || index >= items.length)
        return;

    var item = items[index];
    var updatedNameSurname = prompt('Wprowadź imię i nazwisko: ', item.name_surname);
    var updatedEmail = prompt('Wprowadź adres e-mail: ', item.email);
    var updatedPhoneNumber = prompt('Wprowadź numer telefonu: ', item.phone_number);
    var updatedMessage = prompt('Wprowadź nową treść wiadomości (zmiana tematu formularza niedozwolona): ', item.message);

    if (updatedNameSurname && updatedEmail && updatedPhoneNumber && updatedMessage) {
        item.name_surname = updatedNameSurname;
        item.email = updatedEmail;
        item.phone_number = updatedPhoneNumber;
        item.message = updatedMessage;

        localStorage.setItem('savedFormData', JSON.stringify(items));

        displaySavedFormData();
    }
}

function removeSavedFormData(index) {
    var existingItems = localStorage.getItem('savedFormData');
    if (!existingItems)
        return;

    var items = JSON.parse(existingItems);
    if (index < 0 || index >= items.length)
        return;

    items.splice(index, 1);
    localStorage.setItem('savedFormData', JSON.stringify(items));

    displaySavedFormData();
}

function clearFormData() {
    localStorage.removeItem('savedFormData');
    document.getElementById('tableFormData').innerHTML = '';
}

function hideElementById(id) {
    var element = document.getElementById(id);
    if (element) {
        element.style.display = 'none';
    }
}

function showElementById(id) {
    var element = document.getElementById(id);
    if (element) {
        element.style.display = 'block';
    }
}
