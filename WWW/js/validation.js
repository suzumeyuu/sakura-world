function check_field(field_id, objectRegex) {
    var objectField = document.getElementById(field_id);

    if(!objectRegex.test(objectField.value))
        return false;

    else
        return true;
}

function check_radio(name_radio){
    var object=document.getElementsByName(name_radio);
    var checked = "";

    for (i=0;i<object.length;i++)
    {
        checked = object[i].checked;
        if (checked)
            return true;
    }
    return false;
}

function check_box(box_id)
{
    var object=document.getElementById(box_id);

    if (object.checked)
        return true;
    else
        return false;
}

function validateData() {
    var everythingGood = true;

    var nameSurnameValidator = /^[A-Za-z\sĄąĘęĆćÓóŚśŹźŻżŁł]{7,}$/;
    var emailValidator = /^[A-Za-z.0-9]{5,30}@[A-Za-z0-9]{2,10}.[A-Za-z]{2,3}$/;
    var phoneNumberValidator = /^[0-9]{3}-[0-9]{3}-[0-9]{3}$/;
    var messageValidator = /^[A-Za-zĄąĘęĆćÓóŚśŹźŻżŁł0-9\s.,?!@#$%^&*()-_=+~"':]{30,}$/;

    if(!check_field("name_surname", nameSurnameValidator)) {
        everythingGood = false;
        alert("Wprowadzone imię i nazwisko nie może zawierać cyfr, znaków specjalnych, a także musi mieć minimum 7 znaków. Sprawdź pisownię i spróbuj ponownie.");
    }

    if(!check_field("email", emailValidator)) {
        everythingGood = false;
        alert("Wprowadzony adres e-mail musi być w następującym formacie: 'example@example.com'. Sprawdź pisownię i spróbuj ponownie.");
    }

    if(!check_field("message", messageValidator)) {
        everythingGood = false;
        alert("Wiadomość musi zawierać minimum 30 znaków. Sprawdź pisownię i spróbuj ponownie.");
    }

    var radioValue = document.getElementsByName("want_add_phone_number");
    var checkedRadio = "";

    for (let i=0;i <radioValue.length;i++)
        if(radioValue[i].checked)
            checkedRadio = radioValue[i].value;

    if(checkedRadio === "yes" && !check_field("phone_number", phoneNumberValidator)) {
        everythingGood = false;
        alert("Wprowadzony numer telefonu musi być w następującym formacie: '123-456-789'. Sprawdź pisownię i spróbuj ponownie.");
    }

    if(!check_box("checkbox")) {
        everythingGood = false;
        alert("Pole z oświadczeniem jest wymagane.");
    }

    if(!check_radio("want_add_phone_number")) {
        everythingGood = false;
        alert("Musisz wybrać jedną z opcji ('Chcę podać swój numer telefonu' lub 'Nie chcę podawać numeru telefonu'.");
    }

    if(everythingGood) {
        showFormData();
        saveFormData();
    }

    return everythingGood;
}

function validateDeliveryData() {
    var everythingGood = true;

    var nameSurnameValidator = /^[A-Za-z\sĄąĘęĆćÓóŚśŹźŻżŁł]{7,}$/;
    //var addressValidator = /^[A-Za-z\sĄąĘęĆćÓóŚśŹźŻżŁł.0-9]{7,}$/;
    var emailValidator = /^[A-Za-z.0-9]{5,30}@[A-Za-z0-9]{2,10}.[A-Za-z]{2,3}$/;
    var phoneNumberValidator = /^[0-9]{3}-[0-9]{3}-[0-9]{3}$/;
    var zipCodeValidator = /^[0-9]{2}-[0-9]{3}$/;
    //var cityValidator = /^[A-Za-z\sĄąĘęĆćÓóŚśŹźŻżŁł]{2,}$/;

    if(!check_field("name_surname", nameSurnameValidator)) {
        everythingGood = false;
        alert("Wprowadzone imię i nazwisko nie może zawierać cyfr, znaków specjalnych, a także musi mieć minimum 7 znaków. Sprawdź pisownię i spróbuj ponownie.");
    }

    if(!check_field("email", emailValidator)) {
        everythingGood = false;
        alert("Wprowadzony adres e-mail musi być w następującym formacie: 'example@example.com'. Sprawdź pisownię i spróbuj ponownie.");
    }

    if(!check_field("phone_number", phoneNumberValidator)) {
        everythingGood = false;
        alert("Wprowadzony numer telefonu musi być w następującym formacie: '123-456-789'. Sprawdź pisownię i spróbuj ponownie.");
    }

    /*if(!check_field("address", addressValidator)) {
        everythingGood = false;
        alert("Wprowadzony adres musi mieć minimum 7 znaków. Sprawdź pisownię i spróbuj ponownie.");
    }*/

    if(!check_field("zip_code", zipCodeValidator)) {
        everythingGood = false;
        alert("Wprowadzony kod pocztowy musi być w następującym formacie: '12-345'. Sprawdź pisownię i spróbuj ponownie.");
    }

    /*if(!check_field("city", cityValidator)) {
        everythingGood = false;
        alert("Wprowadzone miasto musi mieć minimum 2 znaki. Sprawdź pisownię i spróbuj ponownie.");
    }*/

    if(everythingGood) {
        showFormDeliveryData();
        saveFormDeliveryData();
        showSite(10);
    }

    return everythingGood;
}
