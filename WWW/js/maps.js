function showLocation(position) {
    var latitude = 51.274834511571406; //position.coords.latitude;
    var longitude = 22.50387582686888; //position.coords.longitude;
    var output = document.getElementById("geo");
    
    //output.innerHTML = "<p>Szerokość geograficzna: " + latitude + "</p>" +
    //                   "<p>Długość geograficzna: " + longitude + "</p>";

    var shopCoords = new google.maps.LatLng(latitude, longitude);
    
    var mapOptions = {
        zoom: 10,
        center: shopCoords,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}
function errorHandler(error) {
    var output = document.getElementById("geo");
    
    switch (error.code) {
        case error.PERMISSION_DENIED:
            output.innerHTML = "Użytkownik nie udostępnił danych.";
            break;
        case error.POSITION_UNAVAILABLE:
            output.innerHTML = "Dane lokalizacyjne niedostępne.";
            break;
        case error.TIMEOUT:
            output.innerHTML = "Przekroczono czas żądania.";
            break;
        case error.UNKNOWN_ERROR:
            output.innerHTML = "Wystąpił nieznany błąd.";
            break;
    }
}

function getLocation() {
    if (navigator.geolocation) {
        var options = {timeout: 60000};
        
        return navigator.geolocation.getCurrentPosition(
            showLocation,
            errorHandler,
            options);
    } 
    else
        alert("Twoja przeglądarka nie wspiera geolokalizacji!");
}
